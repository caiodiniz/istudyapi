const JWT = require('jsonwebtoken'),
    randomatic = require('randomatic'),
    ObjectID = require('mongodb').ObjectID,
    jwt = require('jsonwebtoken'),
    axios = require('axios'),
    User = require('../models/userDAO'),
    { sendEmail } = require('../utils/mailer'),
    { toReal, toFloat } = require('../utils/helperFunctions');

const {
    MY_EMAIL
} = process.env;

const verifyUser = (email, nickname) => {
    return User.find(
        email
    ).then(doc => {
        if (doc) {
            if (doc.length !== 0) {
                let canDelete = true;
                for (let i = 0; i <= doc.length; i++) {
                    if (i !== doc.length) {
                        if (doc[i] && (doc[i].isEmailVerified || doc[i].password !== undefined)) {
                            canDelete = false;
                            if (doc[i].email === email) {
                                canDelete = { message: "Email ja existe", user: false };
                            } else {
                                canDelete = { message: "Nickname ja existe", user: false };
                            }
                        }
                    } else {
                        return { message: canDelete, user: doc[0] };
                    }
                }
            } else {
                return { message: true, user: false };
            }
        } else {
            return { message: "Perda de conexão com servidor", user: user };
        }
    }).catch(err => {
        console.log(err)
        return { message: false, user: user };
    });
}

const getUser = (login) => {
    return User.findOne({
        $or: [
            { email: login },
            { nickname: login }
        ]
    }).then(doc => {
        return doc;
    }).catch(err => {
        console.log(err);
        return false;
    });
}

const getUserById = (_id) => {
    return User.findOne({
        _id
    }).then(user => {
        return user;
    }).catch(err => {
        console.log(err);
        return false;
    });
}



module.exports.register = async function (application, req, res, next) {
    const usuario = req.body;
    const userBD = new User(usuario);
    userBD.save()
        .then(() => {
            res.status(200).json({message: "Sucesso ao criar usuário"})            
        }).catch(err => {
            console.log('(409){Post} /user/register');
            res.status(409).json({ message: "Email ja existe" });
        });
}

module.exports.login = async function (application, req, res, next) {
    let { email, password, role } = req.body;
    if (!email || !password || !role) {
        console.log('(400){Post} /user/email');
        res.status(400).json({
            message: 'Authentication failed',
            error: 'Missing one of these fields: email, password, role'
        });
    } else {
        User.findOne({
            email 
        }).then(user => {
            if (user && user.validPassword(password) && user.hasRole(role)) {
                const payload = {
                    id: user.id,
                    role
                }
                console.log('(200){Post} /user/login');
                token = jwt.sign(payload, process.env.JWT_SECRET);
                id = user._id;
                name = user.name;
                role = user.roles[(user.roles.length - 1)];
                res.status(200).json({ name, id, token, role });
            } else {
                console.log('(400){post} /user/login');
                res.status(400).json({
                    message: 'Authentication failed',
                    error: 'Wrong email or password'
                });
            }
        }).catch(err => {
            console.log(err);
            console.log('(400){post} /user/login');
            res.status(400).json({
                message: 'Authentication failed',
                error: 'Wrong email or password'
            });
        });
    }
}

module.exports.changePassword = async function (application, req, res, next) {
    let formData = req.body;
    const user = await getUserById(req.user.id);

    if (user) {
        if (user && user.password && user.validPassword(formData.oldPassword)) {
            const usuario = {
                password: user.generateHash(formData.password),
                registerCode: null
            }
            User
                .updateOne(
                    { _id: user._id },
                    { $set: usuario }
                ).then(async (user) => {
                    console.log('(200){Post} /user/changePassword');
                    res.status(200).json({ message: "Senha recuperada com sucesso" });
                }).catch(err => {
                    console.log(err);
                    console.log('(500){Post} /user/changePassword');
                    res.status(500).json({ message: "Perda de conexão com servidor" });
                });
        } else {
            console.log('(400){Post} /user/changePassword');
            res.status(400).json({ message: "Senha antiga inválida" });
        }
    } else {
        console.log('(401){Post} /user/changePassword');
        res.status(401).json({ message: "Perda de sessão" });
    }
}