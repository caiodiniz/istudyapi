const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const addressSchema = new Schema({
  street: {type: String, required: true},
  district: {type: String, required: true},
  number: {type: String, required: true},
  city: {type: String, required: true},
  state: {type: String, required: true},
  country: {type: String, required: true},
  cep: {type: String, required: true},	
  user:  {
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    }
  },
},{timestamps: true});

module.exports = mongoose.model('Address', addressSchema);