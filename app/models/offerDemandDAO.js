const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const offerDemandSchema = new Schema({
  user:  {
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    }
  },
  title: {type: String, required: true},
  content: {type: String, required: true},
  time: {type: String, required: true},
  valuePerHour: {type: String, required: true},
	type: {
    type: String,
    enum: ['offer', 'demand']
  },
},{timestamps: true});

module.exports = mongoose.model('OfferDemand', offerDemandSchema);