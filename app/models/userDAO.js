const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const { isEmpty } = require('lodash');
const { SALT_FACTOR, PEPPER } = process.env;

const Schema = mongoose.Schema;

const userSchema = new Schema({
	name: {type: String, required: true, unique: true},
	document: {
		type: {
			type: String,
			enum: ['cpf', 'rg']
		},
		number: String
	},
	phone: {
		DDD: String,
		number: String
	},
	email: {type: String, required: true, unique: true},
	birthday: Date,
	roles: [{
    type: String,
    enum: ['student', 'professor'], 
    required: true
  }],
	password: {
    type: String,
    required: true
  },
},{timestamps: true});


userSchema.methods.hasRole = function (role) {
  if (!isEmpty(this.roles)) {
    return this.roles.find(r => r === role);
  }
  return false;
}

userSchema.methods.generateHash = function (password) {
  return bcrypt.hashSync(password + PEPPER, bcrypt.genSaltSync(SALT_FACTOR), null);
};

userSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password + PEPPER, this.password)
}

userSchema.pre('save', function (next) {
  const user = this;

  if (!user.isModified('password')) return next();

  bcrypt.genSalt(SALT_FACTOR, function (err, salt) {
    if (err) return next(err);

    bcrypt.hash(user.password + PEPPER, salt, function (err, hash) {
      if (err) return next(err);
      user.password = hash;
      next();
    })
  });
});

module.exports = mongoose.model('User', userSchema);