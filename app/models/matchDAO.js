const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const matchSchema = new Schema({
  student:  {
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true
    }
  },
  professor:  {
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true
    }
  },
  offerDemand:  {
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'OfferDemand',
      required: true
    }
  },
  requestDate: {type: Date, required: true, default: new Date()},
  hours: {type: String, required: true},
  confirmDate: {type: Date},
  status: {
    type: String,
    enum: ['accept', 'pendent', 'denied'],
    default: 'pendent'
  },
},{timestamps: true});

module.exports = mongoose.model('Match', matchSchema);