const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const paymentSchema = new Schema({
  match:  {
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Match',
      required: true
    }
  },
  method: {
    type: String,
    enum: ['creditCard', 'money'],
    default: 'pendent',
    required: true
  },
  value: {type: String, required: true},
  status: {
    type: String,
    enum: ['approved', 'pendent', 'denied'],
    default: 'pendent'
  },
},{timestamps: true});

module.exports = mongoose.model('Payment', paymentSchema);