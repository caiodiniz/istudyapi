const auth_token = require('../middlewares/authentication/token/auth_token');
const auth_manager = require('../middlewares/authentication/adm/auth_manager');

module.exports = function(application){	
	application.post('/api/user/register', function(req, res, next){
		application.app.controllers.user.register(application, req, res, next);
	});

	application.post('/api/user/login', function(req, res, next){
		application.app.controllers.user.login(application, req, res, next);
	});

	application.post('/api/user/changePassword', auth_token, function(req, res, next){
		application.app.controllers.user.changePassword(application, req, res, next);
	});
}