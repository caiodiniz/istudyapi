const passportJWT = require('passport-jwt');
const User = require('../../../models/userDAO');

const jwtOptions = {};
jwtOptions.jwtFromRequest = passportJWT.ExtractJwt.fromAuthHeaderWithScheme('JWT');
jwtOptions.secretOrKey = process.env.JWT_SECRET;

module.exports = new passportJWT.Strategy(
  jwtOptions,
  (jwtPayload, done) => {
    User.updateOne({_id: jwtPayload.id}, {
      $set: { lastAccess: new Date() }
    })
      .then(() => {
        User.findOne({_id: jwtPayload.id})
          .then((user) => {
            if (user) {
              user.activeRole = jwtPayload.role;
              done(null, user);
            } else {
              console.log('User nao existe')
              done(null, false);
            }
          })
          .catch((err) => done(err, false));
      })
      .catch((err) => done(err, false));
});
