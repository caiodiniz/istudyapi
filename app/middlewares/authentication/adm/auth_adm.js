module.exports = async (req, res, next) => {
	const User = require('../../../models/userDAO');
	const _id = req.user.id;


	User.findOne({
		$and: [
			{_id},
			{roles: 'admin'}
		]
	}).then(usuario => {

		if(usuario == undefined){
			res.status(401).json({message: "Usuário sem permissão"})
		}else{
			next();
		}


	}).catch(err => {
		console.error(err);
	});
}