const { createTransport } = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');
const path = require('path');

const {
  MAIL_HOST,
  MAIL_USER,
  MAIL_PASSWORD,
  MAIL_PORT,
  MAIL_SECURE
} = process.env;

module.exports.sendEmail = (to, code, cb) => {
  const transporter = createTransport({
    host: MAIL_HOST,
    port: MAIL_PORT,
    secure: MAIL_SECURE,
    auth: {
      user: MAIL_USER,
      pass: MAIL_PASSWORD
    }
  });

  const message = {
    from: 'ZINID App', // sender address
    to, // list of receivers
    subject: 'Confirmação de código ✔',
    text: "Código de verificação",
    html: "<p><h2><b>Código:</b>" + code + "</h2></p>",
  };

  transporter.sendMail(message, (error, info) => {
    transporter.close();
    cb(error, info);
  });
}
